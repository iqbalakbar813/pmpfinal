@extends('layouts.template') 
@section('content')

<!-- Page Title Start -->
<section class="bgtentang">
    <div class="containertentang">
        <img class="gmbrbgtentang" style="height: 10%;" src="{{ url ('assets/images/index5/PROFILE PHOTO 5.png') }}"/>
        <div>
            <img style="height:auto;" src="{{ url ('assets/images/index5/about_us_header.png') }}"/>
        </div>
    </div>
</section>

<!-- About Start -->
<section class="loc_about_wrapper_about" id="tentang">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 mb_30">
            <h5>{{__('bahasa.tentang kami')}}</h5>
            <br>
            <div class="loc_about_text " align="justify">
                 <p>{{__('bahasa.detail_tentang_kami_1')}}</p>
                 <p>{{__('bahasa.detail_tentang_kami_2')}}</p>
                 <p>{{__('bahasa.detail_tentang_kami_3')}}</p>
            </div> 
        </div>
    </div>
</section>

@endsection